import csv
import random
from string import ascii_lowercase


DOWN = 'd'
DOWN_LEFT = 'dl'
DOWN_RIGHT = 'dr'
EMPTY_SPACE = '-'
LEFT = 'l'
MAX_ROWS = 10
MAX_COLUMNS = 10
RIGHT = 'r'
UP = 'u'
UP_LEFT = 'ul'
UP_RIGHT = 'ur'


class GenerationError(Exception):
    pass


def get_speeds(direction):
    x_speed = y_speed = 0
    if LEFT in direction:
        x_speed = -1
    elif RIGHT in direction:
        x_speed = 1
    if UP in direction:
        y_speed = -1
    elif DOWN in direction:
        y_speed = 1
    return x_speed, y_speed


def fit_word(board, word, row, column, direction):
    x_speed, y_speed = get_speeds(direction)
    x = column
    y = row
    start = (y, x)
    for character in word:
        board[y][x] = character
        x += x_speed
        y += y_speed
    x -= x_speed
    y -= y_speed
    return (start[0], start[1], y, x)


def will_fit(board, word, row, column, direction):
    x_speed, y_speed = get_speeds(direction)
    x = column
    y = row
    possible_match = []
    for character in word:
        if not (0 <= x < MAX_COLUMNS) or not (0 <= y < MAX_ROWS) or (board[y][x] not in [EMPTY_SPACE, character]):
            return False
        x += x_speed
        y += y_speed
    return True


def _generate_board(board, words, rows, columns):
    coordinates = {}
    for word in words:
        found = False
        tries = 0
        available_spots = []
        for row in range(rows):
            for column in range(columns):
                available_spots.append((row, column))
        random.shuffle(available_spots)
        while not found and available_spots:
            row, column = available_spots.pop()
            if board[row][column] in [EMPTY_SPACE, word[0]]:
                directions = [UP, DOWN, LEFT, RIGHT, UP_LEFT, UP_RIGHT,
                              DOWN_LEFT, DOWN_RIGHT]
                random.shuffle(directions)
                while len(directions):
                    direction = directions.pop()
                    if will_fit(board, word, row, column, direction):
                        coords = fit_word(board, word, row, column, direction)
                        coordinates[word] = coords
                        found = True
                        break
        if not found:
            raise GenerationError('Could not place the word on the board')
    for row in board:
        for i, character in enumerate(row):
            if character == EMPTY_SPACE:
                row[i] = random.choice(ascii_lowercase)
    return {'board': board, 'coordinates': coordinates}


def generate_board(words, rows=None, columns=None):
    if rows is None:
        rows = MAX_ROWS
    if columns is None:
        columns = MAX_COLUMNS
    for word in words:
        length = len(word)
        if length > rows or length > columns:
            raise GenerationError("%s has a length of %s which is bigger than the board" % (word, length))
    failures = 0
    max_errors = 10
    while failures < max_errors:
        board = [[EMPTY_SPACE for i in range(columns)] for j in range(rows)]
        try:
            board = _generate_board(board, words, rows, columns)
        except GenerationError as e:
            failures += 1
            if failures >= max_errors:
                raise GenerationError('Could not generate the board')
        else:
            break
    return board


if __name__ == '__main__':
    words = ['copper','explain','fated','truck','neat','unite','branch',
             'educated','tenuous','hum','decisive','notice']
    data = generate_board(words)
    print(data)
