from __future__ import print_function
from datetime import datetime
import json
import sys

from bs4 import BeautifulSoup
from flask import Flask, render_template
import requests

from word_search import generate_board


APP = Flask(__name__)
BASE_URL = 'http://family_api.ericbulloch.com/word-search/'


@APP.route('/')
def index():
    global BASE_URL
    response = requests.get(BASE_URL)
    data = response.json()
    return render_template('index.html', weeks=data['weeks'])


@APP.route('/crossword/<_id>')
def crossword(_id):
    try:
        _id = int(_id)
    except:
        abort(404)
    if _id < 1 or _id > 30:
        abort(404)
    return render_template('search.html', user="ashardaylon", id=_id)


@APP.route('/search/<user>/<_id>')
def search(user, _id):
    global BASE_URL
    response = requests.get(BASE_URL + user + '/' + _id)
    return json.dumps(response.json())


if __name__ == "__main__":
    APP.run(host='0.0.0.0')

